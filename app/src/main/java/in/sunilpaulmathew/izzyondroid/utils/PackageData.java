package in.sunilpaulmathew.izzyondroid.utils;

import android.content.Context;
import android.os.Build;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import in.sunilpaulmathew.sCommon.Utils.sJSONUtils;
import in.sunilpaulmathew.sCommon.Utils.sUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class PackageData {

    public static List<RecyclerViewItems> getRawData() {
        JSONArray mPhoneScreenshots = null;
        List<RecyclerViewItems> mData = new ArrayList<>();
        String mTitle, mImageUrl;
        try {
            for (int i = 0; i < Objects.requireNonNull(getApps()).length(); i++) {
                try {
                    JSONObject apps = Objects.requireNonNull(getApps()).getJSONObject(i);
                    if (getAppName(apps.toString()) != null) {
                        mTitle = getAppName(apps.toString());
                    } else {
                        mTitle = getName(apps.toString());
                    }
                    if (getIcon(apps.toString()) != null) {
                        mImageUrl = "https://apt.izzysoft.de/fdroid/repo/icons/" + getIcon(apps.toString());
                    } else {
                        mImageUrl = "https://android.izzysoft.de/frepo/" + getAppPackageName(apps.toString()) + "/en-US/icon.png";
                    }
                    if (getScreenshots(apps.toString()) != null) {
                        mPhoneScreenshots = getScreenshots(apps.toString());
                    }
                    mData.add(new RecyclerViewItems(
                            apps,
                            getAuthorName(apps.toString()),
                            getAuthorEmail(apps.toString()),
                            getAuthorWebsite(apps.toString()),
                            getAppDescription(apps.toString()),
                            getDonationLink(apps.toString()),
                            getLicense(apps.toString()),
                            getAddedDate(apps.toString()),
                            getLastUpdated(apps.toString()),
                            getAppPackageName(apps.toString()),
                            getSourceCode(apps.toString()),
                            getSummary(apps.toString()),
                            mTitle,
                            mImageUrl,
                            getSuggestedVersion(apps.toString()),
                            getChangeLogs(apps.toString()),
                            mPhoneScreenshots)
                    );
                } catch (JSONException ignored) {
                }
            }
        } catch (NullPointerException ignored) {}
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Collections.sort(mData, (lhs, rhs) -> Long.compare(rhs.getLastUpdated(), lhs.getLastUpdated()));
        }
        return mData;
    }

    public static boolean isCategoryApp(String category, JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            try {
                if (array.get(i).equals(category)) {
                    return true;
                }
            } catch (JSONException ignored) {
            }
        }
        return false;
    }

    private static JSONArray getApps() {
        if (Common.getJSONObject() != null) {
            return sJSONUtils.getJSONArray(Common.getJSONObject(), "apps");
        }
        return null;
    }

    static JSONArray getCategories(String string) {
        if (string != null) {
            return sJSONUtils.getJSONArray(sJSONUtils.getJSONObject(string), "categories");
        }
        return null;
    }

    private static long getAddedDate(String string) {
        return sJSONUtils.getLong(sJSONUtils.getJSONObject(string), "added");
    }

    private static String getDonationLink(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(string), "donate");
    }

    private static long getLastUpdated(String string) {
        return sJSONUtils.getLong(sJSONUtils.getJSONObject(string), "lastUpdated");
    }

    private static JSONArray getScreenshots(String string) {
        return sJSONUtils.getJSONArray(sJSONUtils.getJSONObject(getEnglish(string)), "phoneScreenshots");
    }

    private static String getSourceCode(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(string), "sourceCode");
    }

    private static String getSummary(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(getEnglish(string)), "summary");
    }

    private static String getChangeLogs(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(getEnglish(string)), "whatsNew");
    }

    private static String getAppDescription(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(getEnglish(string)), "description");
    }

    private static String getSuggestedVersion(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(string), "suggestedVersionCode");
    }

    private static String getAppName(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(string), "name");
    }

    private static String getAuthorName(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(string), "authorName");
    }

    private static String getAuthorEmail(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(string), "authorEmail");
    }

    private static String getAuthorWebsite(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(string), "authorWebSite");
    }

    private static String getLicense(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(string), "license");
    }

    private static String getAppPackageName(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(string), "packageName");
    }

    private static String getIcon(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(string), "icon");
    }

    private static String getLocalizations(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(string), "localized");
    }

    private static String getEnglish(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(getLocalizations(string)), "en-US");
    }

    private static String getName(String string) {
        return sJSONUtils.getString(sJSONUtils.getJSONObject(getEnglish(string)), "name");
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static void acquireRepoData(Context context) {
        if (Common.isUpdateTime(context)) {
            try (InputStream is = new URL("https://gitlab.com/IzzyOnDroid/iodrepo_binary_transparency_log/-/raw/master/repo/index-v1.json?inline=false").openStream()) {
                BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
                String jsonText = readAll(rd);
                Common.setJSONObject(jsonText);
                sUtils.create(Common.getJSONObject().toString(), Common.getIndexFile(context));
                Common.setRawData(getRawData());
            } catch (IOException ignored) {
            }
        } else {
            Common.setJSONObject(sUtils.read(Common.getIndexFile(context)));
            Common.setRawData(getRawData());
        }
    }

}