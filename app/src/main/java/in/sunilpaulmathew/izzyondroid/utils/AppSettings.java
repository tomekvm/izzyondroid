package in.sunilpaulmathew.izzyondroid.utils;

import android.content.Context;

import com.google.android.material.textview.MaterialTextView;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.sCommon.Utils.sSingleChoiceDialog;
import in.sunilpaulmathew.sCommon.Utils.sUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on November 11, 2021
 */
public class AppSettings {

    private static int getUpdateIntervalPosition(Context context) {
        int interval = sUtils.getInt("updateInterval", 24, context);
        for (int i = 0; i < getUpdateIntervalOptions(context).length; i++) {
            if (Integer.parseInt(getUpdateIntervalOptions(context)[i].replace(" hour","")) == interval) {
                return i;
            }
        }
        return 0;
    }

    private static int getTimeFramePosition(Context context) {
        long timeFrame = sUtils.getLong("latestTimeFrame", 240, context);
        for (int i = 0; i < getTimeFrameOptions(context).length; i++) {
            if (Integer.parseInt(getTimeFrameOptions(context)[i].replace(" hour","")) == timeFrame) {
                return i;
            }
        }
        return 0;
    }

    public static sSingleChoiceDialog setUpdateInterval(MaterialTextView summary, Context context) {
        return new sSingleChoiceDialog(R.drawable.ic_update, context.getString(R.string.update_check_interval), getUpdateIntervalOptions(context),
                getUpdateIntervalPosition(context), context) {
            @Override
            public void onItemSelected(int position) {
                switch (position) {
                    case 0:
                        sUtils.saveInt("updateInterval", 0, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "0"));
                        break;
                    case 1:
                        sUtils.saveInt("updateInterval", 1, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "1"));
                        break;
                    case 2:
                        sUtils.saveInt("updateInterval", 3, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "3"));
                        break;
                    case 3:
                        sUtils.saveInt("updateInterval", 6, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "6"));
                        break;
                    case 4:
                        sUtils.saveInt("updateInterval", 12, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "12"));
                        break;
                    case 5:
                        sUtils.saveInt("updateInterval", 24, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "24"));
                        break;
                    case 6:
                        sUtils.saveInt("updateInterval", 48, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "48"));
                        break;
                }
            }
        };
    }

    public static sSingleChoiceDialog setTimeFrame(MaterialTextView summary, Context context) {
        return new sSingleChoiceDialog(R.drawable.ic_update, context.getString(R.string.latest_timeframe), getTimeFrameOptions(context),
                getTimeFramePosition(context), context) {
            @Override
            public void onItemSelected(int position) {
                switch (position) {
                    case 0:
                        sUtils.saveLong("latestTimeFrame", 24, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "24"));
                        break;
                    case 1:
                        sUtils.saveLong("latestTimeFrame", 48, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "48"));
                        break;
                    case 2:
                        sUtils.saveLong("latestTimeFrame", 72, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "72"));
                        break;
                    case 3:
                        sUtils.saveLong("latestTimeFrame", 96, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "96"));
                        break;
                    case 4:
                        sUtils.saveLong("latestTimeFrame", 120, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "120"));
                        break;
                    case 5:
                        sUtils.saveLong("latestTimeFrame", 240, context);
                        summary.setText(context.getString(R.string.update_check_interval_summary, "240"));
                        break;
                }
            }
        };
    }

    private static String[] getUpdateIntervalOptions(Context context) {
        return new String[] {
                context.getString(R.string.update_check_interval_summary, "0"),
                context.getString(R.string.update_check_interval_summary, "1"),
                context.getString(R.string.update_check_interval_summary, "3"),
                context.getString(R.string.update_check_interval_summary, "6"),
                context.getString(R.string.update_check_interval_summary, "12"),
                context.getString(R.string.update_check_interval_summary, "24"),
                context.getString(R.string.update_check_interval_summary, "48")
        };
    }

    private static String[] getTimeFrameOptions(Context context) {
        return new String[] {
                context.getString(R.string.update_check_interval_summary, "24"),
                context.getString(R.string.update_check_interval_summary, "48"),
                context.getString(R.string.update_check_interval_summary, "72"),
                context.getString(R.string.update_check_interval_summary, "96"),
                context.getString(R.string.update_check_interval_summary, "120"),
                context.getString(R.string.update_check_interval_summary, "240")
        };
    }

}