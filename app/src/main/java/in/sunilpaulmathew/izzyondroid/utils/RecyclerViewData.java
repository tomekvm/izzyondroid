package in.sunilpaulmathew.izzyondroid.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.view.View;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.adapters.CategoryAppsAdapter;
import in.sunilpaulmathew.izzyondroid.adapters.LatestAppsAdapter;
import in.sunilpaulmathew.sCommon.Utils.sExecutor;
import in.sunilpaulmathew.sCommon.Utils.sPackageUtils;
import in.sunilpaulmathew.sCommon.Utils.sUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class RecyclerViewData {

    private static int getOrientation(Activity activity) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && activity.isInMultiWindowMode() ?
                Configuration.ORIENTATION_PORTRAIT : activity.getResources().getConfiguration().orientation;
    }

    public static int getSpanCount(int lSpan, int pSpan, Activity activity) {
        return getOrientation(activity) == Configuration.ORIENTATION_LANDSCAPE ? lSpan : pSpan;
    }

    public static List<RecyclerViewItems> getCategories(String category) {
        List<RecyclerViewItems> mData = new ArrayList<>();
        for (RecyclerViewItems item : Common.getRawRawDate()) {
            if (PackageData.isCategoryApp(category, PackageData.getCategories(item.getRawData().toString()))) {
                if (Common.getSearchText() != null) {
                    if (Common.isTextMatched(item.getTitle())) {
                        mData.add(item);
                    }
                } else {
                    mData.add(item);
                }
            }
        }
        return mData;
    }

    public static List<RecyclerViewItems> getDeveloperApps() {
        List<RecyclerViewItems> mData = new ArrayList<>();
        try {
            for (RecyclerViewItems item : Common.getRawRawDate()) {
                if (item.getAuthorName().equals(Common.getAuthorName())) {
                    mData.add(item);
                }
            }
        } catch (NullPointerException ignored) {}
        return mData;
    }

    public static List<RecyclerViewItems> getLatest(Context context) {
        List<RecyclerViewItems> mData = new ArrayList<>();
        long timeFrame = sUtils.getLong("latestTimeFrame", 240, context);
        for (RecyclerViewItems item : Common.getRawRawDate()) {
            if (System.currentTimeMillis() < timeFrame * 60 * 60 * 1000 + item.getLastUpdated()) {
                mData.add(item);
            }
        }
        return mData;
    }

    public static List<RecyclerViewItems> getInstalled(Activity activity) {
        List<RecyclerViewItems> mData = new ArrayList<>();
        for (RecyclerViewItems item : Common.getRawRawDate()) {
            if (sPackageUtils.isPackageInstalled(item.getPackageName(), activity)) {
                mData.add(item);
            }
        }
        return mData;
    }

    public static List<RecyclerViewItems> getSearchData() {
        List<RecyclerViewItems> mData = new ArrayList<>();
        for (RecyclerViewItems item : Common.getRawRawDate()) {
            if (Common.getSearchText() != null) {
                if (Common.isTextMatched(item.getTitle())) {
                    mData.add(item);
                }
            } else {
                mData.add(item);
            }
        }
        return mData;
    }

    public static List<CategoryMenuItems> getCategories(Context context) {
        List<CategoryMenuItems> mData = new ArrayList<>();
        mData.add(new CategoryMenuItems(sUtils.getDrawable(R.drawable.ic_system, context), sUtils.getColor(R.color.colorAccent, context), "System"));
        mData.add(new CategoryMenuItems(sUtils.getDrawable(R.drawable.ic_multimedia, context), sUtils.getColor(R.color.colorRed, context), "Multimedia"));
        mData.add(new CategoryMenuItems(sUtils.getDrawable(R.drawable.ic_connectivity, context), sUtils.getColor(R.color.colorTeal, context), "Connectivity"));
        mData.add(new CategoryMenuItems(sUtils.getDrawable(R.drawable.ic_science, context), sUtils.getColor(R.color.colorBrown, context), "Science & Education"));
        mData.add(new CategoryMenuItems(sUtils.getDrawable(R.drawable.ic_phone, context), sUtils.getColor(R.color.colorBlueGrey, context), "Phone & SMS"));
        mData.add(new CategoryMenuItems(sUtils.getDrawable(R.drawable.ic_development, context), sUtils.getColor(R.color.colorPink, context), "Development"));
        mData.add(new CategoryMenuItems(sUtils.getDrawable(R.drawable.ic_security, context), sUtils.getColor(R.color.colorPurple, context), "Security"));
        mData.add(new CategoryMenuItems(sUtils.getDrawable(R.drawable.ic_network, context), sUtils.getColor(R.color.colorAccent, context), "Internet"));
        mData.add(new CategoryMenuItems(sUtils.getDrawable(R.drawable.ic_reading, context), sUtils.getColor(R.color.colorRed, context), "Reading"));
        mData.add(new CategoryMenuItems(sUtils.getDrawable(R.drawable.ic_office, context), sUtils.getColor(R.color.colorOrange, context), "Office"));
        mData.add(new CategoryMenuItems(sUtils.getDrawable(R.drawable.ic_games, context), sUtils.getColor(R.color.colorBrown, context), "Games"));
        mData.add(new CategoryMenuItems(sUtils.getDrawable(R.drawable.ic_food, context), sUtils.getColor(R.color.colorGreen, context), "Food"));
        return mData;
    }

    public static void loadCategories(String category, LinearLayoutCompat layout, RecyclerView recyclerView, Activity activity) {
        new sExecutor() {

            @Override
            public void onPreExecute() {
                layout.setVisibility(View.VISIBLE);
            }

            @Override
            public void doInBackground() {
                if (Common.getJSONObject() == null) {
                    Common.setJSONObject(sUtils.read(Common.getIndexFile(activity)));
                }
            }

            @Override
            public void onPostExecute() {
                recyclerView.setAdapter(new CategoryAppsAdapter(RecyclerViewData.getCategories(category)));
                layout.setVisibility(View.GONE);
            }
        }.execute();
    }

    public static void loadDeveloperApps(LinearLayoutCompat layout, RecyclerView recyclerView, Activity activity) {
        new sExecutor() {

            @Override
            public void onPreExecute() {
                layout.setVisibility(View.VISIBLE);
            }

            @Override
            public void doInBackground() {
                if (Common.getJSONObject() == null) {
                    Common.setJSONObject(sUtils.read(Common.getIndexFile(activity)));
                }
            }

            @Override
            public void onPostExecute() {
                recyclerView.setAdapter(new CategoryAppsAdapter(RecyclerViewData.getDeveloperApps()));
                layout.setVisibility(View.GONE);
            }
        }.execute();
    }

    public static void loadInstalledApps(LinearLayoutCompat layout, RecyclerView recyclerView, Activity activity) {
        new sExecutor() {

            @Override
            public void onPreExecute() {
                layout.setVisibility(View.VISIBLE);
            }

            @Override
            public void doInBackground() {
                if (Common.getJSONObject() == null) {
                    Common.setJSONObject(sUtils.read(Common.getIndexFile(activity)));
                }
            }

            @Override
            public void onPostExecute() {
                recyclerView.setAdapter(new LatestAppsAdapter(RecyclerViewData.getInstalled(activity)));
                layout.setVisibility(View.GONE);
            }
        }.execute();
    }

    public static void loadLatestApps(LinearLayoutCompat layout, RecyclerView recyclerView, Activity activity) {
        new sExecutor() {

            @Override
            public void onPreExecute() {
                layout.setVisibility(View.VISIBLE);
            }

            @Override
            public void doInBackground() {
                if (Common.getJSONObject() == null) {
                    Common.setJSONObject(sUtils.read(Common.getIndexFile(activity)));
                }
            }

            @Override
            public void onPostExecute() {
                recyclerView.setAdapter(new LatestAppsAdapter(RecyclerViewData.getLatest(activity)));
                layout.setVisibility(View.GONE);
            }
        }.execute();
    }

}