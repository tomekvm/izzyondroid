package in.sunilpaulmathew.izzyondroid.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.activities.PackageViewActivity;
import in.sunilpaulmathew.sCommon.Utils.sUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class Common {

    private static boolean mDownloading;
    private static JSONArray mScreenShots;
    private static JSONObject mJSONObject = null;
    private static int mScreenshotPosition, mTabPosition;
    private static List<RecyclerViewItems> mRawDate = null;
    private static long mAdded, mLastUpdated;
    private static String mAppName, mAppDescription, mAppIconURL, mAppSource, mAppSummary, mAuthorName,
            mAuthorEmail, mAuthorWeb, mChangeLogs, mDonations, mLicence, mPackageName, mSearchText, mVersionCode;

    public static boolean isDownloading() {
        return mDownloading;
    }

    static boolean isTextMatched(String searchText) {
        for (int a = 0; a < searchText.length() - mSearchText.length() + 1; a++) {
            if (mSearchText.equalsIgnoreCase(searchText.substring(a, a + mSearchText.length()))) {
                return true;
            }
        }
        return false;
    }

    public static boolean isUpdateTime(Context context) {
        return System.currentTimeMillis() > Common.getUCTimeStamp(context) + Common.getInterval(context);
    }

    public static File getIndexFile(Context context) {
        return new File(context.getCacheDir(), "index.json");
    }

    private static JSONArray getScreenShots() {
        return mScreenShots;
    }

    public static JSONObject getJSONObject() {
        return mJSONObject;
    }

    public static int getScreenshotPosition() {
        return mScreenshotPosition;
    }

    public static int getTabPosition() {
        return mTabPosition;
    }

    public static List<RecyclerViewItems> getRawRawDate() {
        return mRawDate;
    }

    public static List<String> getScreenshots() {
        List<String> mScreenshots = new ArrayList<>();
        if (getScreenShots() != null) {
            for (int i = 0; i < getScreenShots().length(); i++) {
                try {
                    mScreenshots.add("https://apt.izzysoft.de/fdroid/repo/" + getPackageName() + "/en-US/phoneScreenshots/"
                            + getScreenShots().getString(i));
                } catch (JSONException ignored) {
                }
            }
        }
        return mScreenshots;
    }

    public static long getUCTimeStamp(Context context) {
        return sUtils.getLong("ucTimeStamp", 0, context);
    }

    public static int getInterval(Context context) {
        return sUtils.getInt("updateInterval", 24, context) * 60 * 60 * 1000;
    }

    public static String getAddedTime() {
        return getAdjustedDate(mAdded);
    }

    public static String getAdjustedDate(long date) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(date);

    }

    public static String getAppIconURL() {
        return mAppIconURL;
    }

    public static String getAppName() {
        return mAppName;
    }

    public static String getAppDescription() {
        return mAppDescription;
    }

    public static String getAppSource(Context context) {
        if (getAppSourceUrl().startsWith("https://github.com/")) {
            return context.getString(R.string.github);
        } else if (getAppSourceUrl().startsWith("https://gitlab.com/")) {
            return context.getString(R.string.gitlab);
        } else {
            return context.getString(R.string.click_here);
        }
    }

    public static String getAppSourceUrl() {
        return mAppSource;
    }

    public static String getAppSummary() {
        return mAppSummary;
    }

    public static String getAuthorName() {
        return mAuthorName;
    }

    public static String getAuthorEmail() {
        return mAuthorEmail;
    }

    public static String getAuthorWeb() {
        return mAuthorWeb;
    }

    public static String getLastUpdated() {
        return getAdjustedDate(mLastUpdated);
    }

    public static String getChangeLogs() {
        return mChangeLogs;
    }

    public static String getDonationLink() {
        return mDonations;
    }

    public static String getLicence() {
        return mLicence;
    }

    public static String getPackageName() {
        return mPackageName;
    }

    public static String getSearchText() {
        return mSearchText;
    }

    public static String getVersionCode() {
        return mVersionCode;
    }

    public static void launchPackageView(RecyclerViewItems item, Context context) {
        setAppIconURL(item.getImageUrl());
        setAppName(item.getTitle());
        setPackageName(item.getPackageName());
        setAppDescription(item.getDescription());
        setAuthorName(item.getAuthorName());
        setAppSummary(item.getSummary());
        setAppSourceUrl(item.getSource());
        setVersionCode(item.getVersionCode());
        setScreenShots(item.getPhoneScreenshots());
        setAuthorEmail(item.getAuthorEmail());
        setAuthorWeb(item.getAuthorWeb());
        setChangeLogs(item.getChangeLogs());
        setDonationLink(item.getDonationLink());
        setLicence(item.getLicence());
        setAddedTime(item.getAddedDate());
        setLastUpdated(item.getLastUpdated());
        Intent details = new Intent(context, PackageViewActivity.class);
        context.startActivity(details);
    }

    public static void isDownloading(boolean b) {
        mDownloading = b;
    }

    private static void setAddedTime(long added) {
        mAdded = added;
    }

    private static void setLastUpdated(long updated) {
        mLastUpdated = updated;
    }

    private static void setScreenShots(JSONArray screenShots) {
        mScreenShots = screenShots;
    }

    public static void setJSONObject(String jsonData) {
        try {
            mJSONObject = new JSONObject(jsonData);
        } catch (JSONException | NullPointerException ignored) {
        }
    }

    private static void setAppIconURL(String appIconURL) {
        mAppIconURL = appIconURL;
    }

    private static void setAppName(String name) {
        mAppName = name;
    }

    private static void setAppDescription(String description) {
        mAppDescription = description;
    }

    private static void setAppSummary(String appSummary) {
        mAppSummary = appSummary;
    }

    private static void setAppSourceUrl(String appSourceUrl) {
        mAppSource = appSourceUrl;
    }

    private static void setAuthorName(String authorName) {
        mAuthorName = authorName;
    }

    private static void setAuthorEmail(String authorEmail) {
        mAuthorEmail = authorEmail;
    }

    private static void setAuthorWeb(String authorWeb) {
        mAuthorWeb = authorWeb;
    }

    private static void setChangeLogs(String changeLogs) {
        mChangeLogs = changeLogs;
    }

    private static void setDonationLink(String donations) {
        mDonations = donations;
    }

    private static void setLicence(String licence) {
        mLicence = licence;
    }

    private static void setPackageName(String packageName) {
        mPackageName = packageName;
    }

    public static void setRawData(List<RecyclerViewItems> data) {
        mRawDate = data;
    }

    public static void setScreenshotPosition(int position) {
        mScreenshotPosition = position;
    }

    public static void setSearchText(String searchText) {
        mSearchText = searchText;
    }

    public static void setTabPosition(int position) {
        mTabPosition = position;
    }

    private static void setVersionCode(String versionCode) {
        mVersionCode = versionCode;
    }

}