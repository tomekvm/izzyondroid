package in.sunilpaulmathew.izzyondroid.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewpager.widget.PagerAdapter;

import com.squareup.picasso.Picasso;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.Common;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on September 03, 2021
 */
public class ImageViewAdapter extends PagerAdapter {

    public ImageViewAdapter() {
    }

    @Override
    public int getCount() {
        return Common.getScreenshots().size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        AppCompatImageView imageView = new AppCompatImageView(container.getContext());
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        Picasso.get().load(Common.getScreenshots().get(position)).placeholder(
                R.mipmap.ic_launcher_foreground).into(imageView);
        container.addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
        container.removeView((AppCompatImageView) object);
    }

}