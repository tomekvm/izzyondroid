package in.sunilpaulmathew.izzyondroid.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;

import java.util.List;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.AppSettings;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.PackageData;
import in.sunilpaulmathew.sCommon.Utils.sExecutor;
import in.sunilpaulmathew.sCommon.Utils.sSerializableItems;
import in.sunilpaulmathew.sCommon.Utils.sThemeUtils;
import in.sunilpaulmathew.sCommon.Utils.sTranslatorUtils;
import in.sunilpaulmathew.sCommon.Utils.sUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.ViewHolder> {

    private static List<sSerializableItems> data;
    public SettingsAdapter(List<sSerializableItems> data){
        SettingsAdapter.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_searching, parent, false);
        return new ViewHolder(rowItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.mName.setText(data.get(position).getTextOne());
            holder.mSummary.setText(data.get(position).getTextTwo());
            holder.mIcon.setImageDrawable(data.get(position).getIcon());
            holder.mIcon.setColorFilter(sUtils.getColor(R.color.colorAccent, holder.mIcon.getContext()));
        } catch (IndexOutOfBoundsException | NullPointerException ignored) {}
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AppCompatImageView mIcon;
        private final MaterialTextView mName, mSummary;
        private final ProgressBar mProgress;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            this.mIcon = view.findViewById(R.id.icon);
            this.mName = view.findViewById(R.id.title);
            this.mSummary = view.findViewById(R.id.description);
            this.mProgress = view.findViewById(R.id.progress);
        }

        @Override
        public void onClick(View view) {
            if (data.get(getAdapterPosition()).getTextThree() != null) {
                sUtils.launchUrl(data.get(getAdapterPosition()).getTextThree(), (Activity) view.getContext());
                return;
            }
            switch (getAdapterPosition()) {
                case 0:
                    sThemeUtils.setAppTheme(view.getContext());
                    break;
                case 1:
                    AppSettings.setUpdateInterval(mSummary, view.getContext()).show();
                    break;
                case 2:
                    AppSettings.setTimeFrame(mSummary, view.getContext()).show();
                    break;
                case 3:
                    new sExecutor() {
                        @Override
                        public void onPreExecute() {
                            mName.setVisibility(View.GONE);
                            mSummary.setVisibility(View.GONE);
                            mProgress.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void doInBackground() {
                            PackageData.acquireRepoData(view.getContext());
                        }

                        @Override
                        public void onPostExecute() {
                            if (Common.getJSONObject() != null) {
                                sUtils.saveLong("ucTimeStamp", System.currentTimeMillis(), view.getContext());
                                mSummary.setText(view.getContext().getString(R.string.refresh_repo_summary, Common.getAdjustedDate(sUtils.getLong("ucTimeStamp", 0, view.getContext()))));
                                mProgress.setVisibility(View.GONE);
                                mName.setVisibility(View.VISIBLE);
                                mSummary.setVisibility(View.VISIBLE);
                            }
                        }
                    }.execute();
                    break;
                case 5:
                    new sTranslatorUtils(view.getContext().getString(R.string.app_name), "https://gitlab.com/sunilpaulmathew/izzyondroid/-/blob/master/app/src/main/res/values/strings.xml",
                            (Activity) view.getContext()).show();
                    break;
            }
        }
    }

}