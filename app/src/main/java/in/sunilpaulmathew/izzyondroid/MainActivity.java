package in.sunilpaulmathew.izzyondroid;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textview.MaterialTextView;

import java.util.Objects;

import in.sunilpaulmathew.izzyondroid.activities.WelcomeActivity;
import in.sunilpaulmathew.izzyondroid.fragments.CategoryFragment;
import in.sunilpaulmathew.izzyondroid.fragments.InstalledFragment;
import in.sunilpaulmathew.izzyondroid.fragments.LatestFragment;
import in.sunilpaulmathew.izzyondroid.fragments.SettingsFragment;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.PackageData;
import in.sunilpaulmathew.sCommon.Adapters.sPagerAdapter;
import in.sunilpaulmathew.sCommon.Utils.sCrashReporterUtils;
import in.sunilpaulmathew.sCommon.Utils.sExecutor;
import in.sunilpaulmathew.sCommon.Utils.sThemeUtils;
import in.sunilpaulmathew.sCommon.Utils.sUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class MainActivity extends AppCompatActivity {

    private static boolean mExit;

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sThemeUtils.initializeAppTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new sCrashReporterUtils(sUtils.getColor(R.color.colorAccent, this), 20, this).initialize();

        BottomNavigationView mBottomNav = findViewById(R.id.bottom_navigation);
        LinearLayoutCompat mProgressLayout = findViewById(R.id.progress_layout);
        MaterialTextView mProgressText = findViewById(R.id.progress_text);
        ViewPager mViewPager = findViewById(R.id.view_pager);

        if (Common.getIndexFile(this).exists()) {
            if (Common.getRawRawDate() == null) {
                new sExecutor() {

                    @Override
                    public void onPreExecute() {
                        mBottomNav.setVisibility(View.GONE);
                        mProgressLayout.setVisibility(View.VISIBLE);
                        mProgressText.setText(getString(Common.isUpdateTime(MainActivity.this) ?
                                R.string.updating : R.string.loading));
                    }

                    @Override
                    public void doInBackground() {
                        PackageData.acquireRepoData(MainActivity.this);
                    }

                    @Override
                    public void onPostExecute() {
                        if (Common.isUpdateTime(MainActivity.this) && Common.getJSONObject() != null) {
                            sUtils.saveLong("ucTimeStamp", System.currentTimeMillis(), MainActivity.this);
                        }
                        mProgressLayout.setVisibility(View.GONE);
                        mBottomNav.setVisibility(View.VISIBLE);
                        mViewPager.setAdapter(getAdapter());
                        if (savedInstanceState == null) {
                            mViewPager.setCurrentItem(0);
                        }
                    }
                }.execute();
            } else {
                mViewPager.setAdapter(getAdapter());
                if (savedInstanceState == null) {
                    mViewPager.setCurrentItem(0);
                }
            }

            mBottomNav.setOnItemSelectedListener(
                    menuItem -> {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_latest:
                                mViewPager.setCurrentItem(0);
                                break;
                            case R.id.nav_categories:
                                mViewPager.setCurrentItem(1);
                                break;
                            case R.id.nav_installed:
                                mViewPager.setCurrentItem(2);
                                break;
                            case R.id.nav_settings:
                                mViewPager.setCurrentItem(3);
                                break;
                        }
                        Objects.requireNonNull(mViewPager.getAdapter()).notifyDataSetChanged();
                        return false;
                    }
            );
        } else {
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
            finish();
        }

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int i, final float v, final int i2) {
            }
            @Override
            public void onPageSelected(int position) {
                Objects.requireNonNull(mViewPager.getAdapter()).notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(final int i) {
            }
        });

    }

    private sPagerAdapter getAdapter() {
        sPagerAdapter mAdapter = new sPagerAdapter(getSupportFragmentManager());
        mAdapter.AddFragment(new LatestFragment(), null);
        mAdapter.AddFragment(new CategoryFragment(), null);
        mAdapter.AddFragment(new InstalledFragment(), null);
        mAdapter.AddFragment(new SettingsFragment(), null);
        return mAdapter;
    }

    @Override
    public void onBackPressed() {
        if (mExit) {
            mExit = false;
            super.onBackPressed();
        } else {
            sUtils.snackBar(findViewById(android.R.id.content), getString(R.string.press_back)).show();
            mExit = true;
            new Handler().postDelayed(() -> mExit = false, 2000);
        }
    }

}