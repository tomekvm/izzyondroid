package in.sunilpaulmathew.izzyondroid.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import in.sunilpaulmathew.izzyondroid.BuildConfig;
import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.activities.DeveloperActivity;
import in.sunilpaulmathew.izzyondroid.activities.PackageSearchingActivity;
import in.sunilpaulmathew.izzyondroid.adapters.DetailsAdapter;
import in.sunilpaulmathew.izzyondroid.adapters.ScreenshotsAdapter;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.Installer;
import in.sunilpaulmathew.sCommon.Utils.sAPKUtils;
import in.sunilpaulmathew.sCommon.Utils.sPackageUtils;
import in.sunilpaulmathew.sCommon.Utils.sSerializableItems;
import in.sunilpaulmathew.sCommon.Utils.sUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class PackageViewFragment extends Fragment {

    private LinearLayoutCompat mButtonsLayout;

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_package_view, container, false);

        AppCompatImageButton mBack = mRootView.findViewById(R.id.back);
        AppCompatImageButton mMinimizeDescription = mRootView.findViewById(R.id.minimize_description);
        AppCompatImageButton mMinimizeScreenshot = mRootView.findViewById(R.id.minimize_screenshot);
        AppCompatImageButton mMinimizeWhatsNew = mRootView.findViewById(R.id.minimize_whatsnew);
        AppCompatImageButton mSearch = mRootView.findViewById(R.id.search);
        AppCompatImageButton mShare = mRootView.findViewById(R.id.share);
        AppCompatImageView mAppImage = mRootView.findViewById(R.id.app_image);
        FrameLayout mAboutFrame = mRootView.findViewById(R.id.about_frame);
        FrameLayout mWhatsNewFrame = mRootView.findViewById(R.id.whatsnew_frame);
        FrameLayout mScreenshotFrame = mRootView.findViewById(R.id.screenshot_frame);
        LinearLayoutCompat mDeveloperPage = mRootView.findViewById(R.id.developer_page);
        LinearLayoutCompat mProgressLayout = mRootView.findViewById(R.id.progress_layout);
        mButtonsLayout = mRootView.findViewById(R.id.button_layout);
        MaterialCardView mWhatsNewCard = mRootView.findViewById(R.id.card_whatsnew);
        MaterialCardView mScreenshotCard = mRootView.findViewById(R.id.card_screenshots);
        MaterialCardView mDescriptionCard = mRootView.findViewById(R.id.card_description);
        MaterialCardView mButtonOne = mRootView.findViewById(R.id.button_one);
        MaterialCardView mButtonTwo = mRootView.findViewById(R.id.button_two);
        MaterialCardView mAppInfo = mRootView.findViewById(R.id.app_info_title);
        MaterialTextView mAppTitle = mRootView.findViewById(R.id.app_title);
        MaterialTextView mAuthorName = mRootView.findViewById(R.id.author_name);
        MaterialTextView mDownloadStatus = mRootView.findViewById(R.id.download_status);
        MaterialTextView mAppDescription = mRootView.findViewById(R.id.app_description);
        MaterialTextView mWhatsNew = mRootView.findViewById(R.id.whats_new);
        MaterialTextView mButtonOneTxt = mRootView.findViewById(R.id.button_one_text);
        MaterialTextView mButtonTwoTxt = mRootView.findViewById(R.id.button_two_text);
        MaterialTextView mCancel = mRootView.findViewById(R.id.cancel);
        RecyclerView mRecyclerView = mRootView.findViewById(R.id.recycler_view);
        RecyclerView mRecyclerViewDetails = mRootView.findViewById(R.id.recycler_view_details);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false));
        ScreenshotsAdapter mRecycleViewAdapter = new ScreenshotsAdapter(Common.getScreenshots());
        mRecyclerView.setAdapter(mRecycleViewAdapter);

        mRecyclerViewDetails.setLayoutManager(new LinearLayoutManager(requireActivity()));
        DetailsAdapter mDetailsAdapter = new DetailsAdapter(getDetails());
        mRecyclerViewDetails.setAdapter(mDetailsAdapter);

        mAppTitle.setText(Common.getAppName());
        try {
            mAppDescription.setText(Common.getAppSummary().toUpperCase() + "\n\n" + fromHtml(Common.getAppDescription()));
        } catch (NullPointerException ignored) {}
        Picasso.get().load(Common.getAppIconURL()).into(mAppImage);
        mAuthorName.setText(Common.getAuthorName());

        if (!sUtils.isDarkTheme(requireActivity())) {
            mWhatsNewCard.setCardBackgroundColor(Color.LTGRAY);
            mScreenshotCard.setCardBackgroundColor(Color.LTGRAY);
            mDescriptionCard.setCardBackgroundColor(Color.LTGRAY);
            mAppInfo.setCardBackgroundColor(Color.LTGRAY);
        }

        if (Common.getAppSummary() == null && Common.getAppDescription() == null) {
            mDescriptionCard.setVisibility(View.GONE);
        }

        if (Common.getChangeLogs() != null) {
            mWhatsNew.setText(Common.getChangeLogs());
        } else {
            mWhatsNewCard.setVisibility(View.GONE);
        }

        if (sPackageUtils.isPackageInstalled(Common.getPackageName(), requireActivity())) {
            if (sAPKUtils.getVersionCode(sPackageUtils.getSourceDir(Common.getPackageName(), requireActivity()), requireActivity()) < Integer.parseInt(Common.getVersionCode())) {
                mButtonOneTxt.setText(R.string.update);
                mButtonOneTxt.setTextColor(sUtils.getColor(R.color.colorAccent, requireActivity()));
            } else {
                mButtonOneTxt.setText(R.string.open);
                mButtonOneTxt.setTextColor(Color.GREEN);
            }
            mButtonTwoTxt.setText(R.string.uninstall);
            mButtonTwoTxt.setTextColor(Color.RED);
            mButtonOne.setVisibility(View.VISIBLE);
            mButtonTwo.setVisibility(View.VISIBLE);

            mButtonOne.setOnClickListener(v -> {
                if (sAPKUtils.getVersionCode(sPackageUtils.getSourceDir(Common.getPackageName(), requireActivity()), requireActivity()) < Integer.parseInt(Common.getVersionCode())) {
                    Installer.installPackage(mProgressLayout, mButtonsLayout, mDownloadStatus, "https://apt.izzysoft.de/fdroid/repo/" +
                            Common.getPackageName() + "_" + Common.getVersionCode() + ".apk", new File(requireActivity().getExternalCacheDir(),
                            "tmp.apk"), requireActivity());
                } else {
                    Intent launchIntent = requireActivity().getPackageManager().getLaunchIntentForPackage(Common.getPackageName());
                    if (launchIntent != null) {
                        startActivity(launchIntent);
                        requireActivity().finish();
                    }
                }
            });

            mButtonTwo.setOnClickListener(v -> {
                Intent remove = new Intent(Intent.ACTION_DELETE);
                remove.putExtra(Intent.EXTRA_RETURN_RESULT, true);
                remove.setData(Uri.parse("package:" + Common.getPackageName()));
                startActivityForResult(remove, 0);
            });
        } else {
            mButtonOneTxt.setText(R.string.install);
            mButtonOneTxt.setTextColor(Color.GREEN);
            mButtonOne.setVisibility(View.VISIBLE);

            mButtonOne.setOnClickListener(v -> Installer.installPackage(mProgressLayout, mButtonsLayout, mDownloadStatus, "https://apt.izzysoft.de/fdroid/repo/" +
                    Common.getPackageName() + "_" + Common.getVersionCode() + ".apk", new File(requireActivity().getExternalCacheDir(),
                    "tmp.apk"), requireActivity()));
        }

        mSearch.setOnClickListener(v -> {
            Intent searching = new Intent(requireActivity(), PackageSearchingActivity.class);
            startActivity(searching);
            requireActivity().finish();
        });

        mShare.setOnClickListener(v -> {
            Intent share_app = new Intent();
            share_app.setAction(Intent.ACTION_SEND);
            share_app.putExtra(Intent.EXTRA_SUBJECT, Common.getAppName());
            share_app.putExtra(Intent.EXTRA_TEXT, "https://apt.izzysoft.de/fdroid/index/apk/" + Common.getPackageName()
                    + "\n\n" + getString(R.string.share_message, BuildConfig.VERSION_NAME));
            share_app.setType("text/plain");
            Intent shareIntent = Intent.createChooser(share_app, getString(R.string.share_with));
            startActivity(shareIntent);
        });

        mWhatsNewFrame.setOnClickListener(v -> {
            if (Common.isDownloading()) return;
            if (mWhatsNew.getVisibility() == View.GONE) {
                mWhatsNew.setVisibility(View.VISIBLE);
                mMinimizeWhatsNew.setRotation(180);
            } else {
                mWhatsNew.setVisibility(View.GONE);
                mMinimizeWhatsNew.setRotation(0);
            }
        });

        mAboutFrame.setOnClickListener(v -> {
            if (Common.isDownloading()) return;
            if (mAppDescription.getVisibility() == View.GONE) {
                mAppDescription.setVisibility(View.VISIBLE);
                mMinimizeDescription.setRotation(180);
            } else {
                mAppDescription.setVisibility(View.GONE);
                mMinimizeDescription.setRotation(0);
            }
        });

        mScreenshotFrame.setOnClickListener(v -> {
            if (Common.isDownloading()) return;
            if (mRecyclerView.getVisibility() == View.GONE) {
                mRecyclerView.setVisibility(View.VISIBLE);
                mMinimizeScreenshot.setRotation(180);
            } else {
                mRecyclerView.setVisibility(View.GONE);
                mMinimizeScreenshot.setRotation(0);
            }
        });

        mDeveloperPage.setOnClickListener(v -> {
            Intent developerPage = new Intent(requireActivity(), DeveloperActivity.class);
            startActivity(developerPage);
            requireActivity().finish();
        });

        mBack.setOnClickListener(v -> {
            if (Common.isDownloading()) return;
            requireActivity().finish();
        });

        mCancel.setOnClickListener(v -> {
            if (Common.isDownloading()) return;
            requireActivity().finish();
        });

        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (mButtonsLayout.getVisibility() == View.VISIBLE) {
                    requireActivity().finish();
                } else {
                    sUtils.snackBar(requireActivity().findViewById(android.R.id.content), getString(R.string.downloading_progress));
                }
            }
        });

        return mRootView;
    }

    public List<sSerializableItems> getDetails() {
        List<sSerializableItems> mDetails = new ArrayList<>();
        mDetails.add(new sSerializableItems(null, getString(R.string.package_name), Common.getPackageName(), null));
        if (Common.getAuthorWeb() != null) {
            mDetails.add(new sSerializableItems(null, getString(R.string.author_web), Common.getAuthorWeb(), Common.getAuthorWeb()));
        }
        if (Common.getAuthorEmail() != null) {
            mDetails.add(new sSerializableItems(null, getString(R.string.author_email), Common.getAuthorEmail(), "mailto:" + Common.getAuthorEmail()));
        }
        mDetails.add(new sSerializableItems(null, getString(R.string.source_code), Common.getAppSource(requireActivity()), Common.getAppSourceUrl()));
        mDetails.add(new sSerializableItems(null, getString(R.string.released), Common.getAddedTime(), null));
        mDetails.add(new sSerializableItems(null, getString(R.string.updated), Common.getLastUpdated(), null));
        mDetails.add(new sSerializableItems(null, getString(R.string.licence), Common.getLicence(), null));
        if (Common.getDonationLink() != null) {
            mDetails.add(new sSerializableItems(null, getString(R.string.donations), getString(R.string.donations_Summary), Common.getDonationLink()));
        }
        return mDetails;
    }

    private static CharSequence fromHtml(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0 && data != null && resultCode == Activity.RESULT_OK) {
            requireActivity().recreate();
        }
    }

}