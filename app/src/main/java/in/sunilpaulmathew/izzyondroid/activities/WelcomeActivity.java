package in.sunilpaulmathew.izzyondroid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;

import in.sunilpaulmathew.izzyondroid.MainActivity;
import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.PackageData;
import in.sunilpaulmathew.sCommon.Utils.sExecutor;
import in.sunilpaulmathew.sCommon.Utils.sUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 28, 2021
 */
public class WelcomeActivity extends AppCompatActivity {

    private ProgressBar mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        MaterialCardView mStartCard = findViewById(R.id.start_card);
        MaterialTextView mMainText = findViewById(R.id.main_text);
        mProgress = findViewById(R.id.progress);

        mStartCard.setOnClickListener(v -> new sExecutor() {

            @Override
            public void onPreExecute() {
                mMainText.setText(getString(R.string.initializing));
                mStartCard.setVisibility(View.GONE);
                mProgress.setVisibility(View.VISIBLE);
            }

            @Override
            public void doInBackground() {
                PackageData.acquireRepoData(WelcomeActivity.this);
            }

            @Override
            public void onPostExecute() {
                if (Common.getJSONObject() != null) {
                    sUtils.saveLong("ucTimeStamp", System.currentTimeMillis(), WelcomeActivity.this);
                    Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    sUtils.snackBar(mStartCard, getString(R.string.initializing_failed)).show();
                    mMainText.setText(getString(R.string.welcome_message));
                    mStartCard.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                }
            }
        }.execute());
    }

    @Override
    public void onBackPressed() {
        if (mProgress.getVisibility() == View.GONE) {
            finish();
        }
    }

}