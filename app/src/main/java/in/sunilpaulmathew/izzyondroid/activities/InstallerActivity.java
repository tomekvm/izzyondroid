package in.sunilpaulmathew.izzyondroid.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.sCommon.Utils.sAPKUtils;
import in.sunilpaulmathew.sCommon.Utils.sUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class InstallerActivity extends AppCompatActivity {

    private MaterialCardView mCancel, mOpen;
    private MaterialTextView mStatus;
    private ProgressBar mProgress;
    public static final String PATH_INTENT = "path";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_installer);

        AppCompatImageButton mIcon = findViewById(R.id.icon);
        mProgress = findViewById(R.id.progress);
        mOpen = findViewById(R.id.open);
        mCancel = findViewById(R.id.cancel);
        MaterialTextView mTitle = findViewById(R.id.title);
        mStatus = findViewById(R.id.status);

        String mPath = getIntent().getStringExtra(PATH_INTENT);
        mTitle.setText(getString(R.string.installing, sAPKUtils.getAPKName(mPath,this)));
        mIcon.setImageDrawable(sAPKUtils.getAPKIcon(mPath,this));

        mOpen.setOnClickListener(v -> {
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage(Common.getPackageName());
            if (launchIntent != null) {
                startActivity(launchIntent);
                finish();
            }
        });

        mCancel.setOnClickListener(v -> onBackPressed());

        refreshStatus(this);
    }

    public void refreshStatus(Activity activity) {
        new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(500);
                        runOnUiThread(() -> {
                            String installationStatus = sUtils.getString("installationStatus", "waiting", activity);
                            if (!installationStatus.equals("waiting")) {
                                mStatus.setText(installationStatus);
                                mProgress.setVisibility(View.GONE);
                                mCancel.setVisibility(View.VISIBLE);
                                if (installationStatus.equals(getString(R.string.installation_status_success))) {
                                    mOpen.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }
                } catch (InterruptedException ignored) {}
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        if (sUtils.getString("installationStatus", "waiting", this).equals("waiting")) {
            return;
        }
        super.onBackPressed();
    }

}