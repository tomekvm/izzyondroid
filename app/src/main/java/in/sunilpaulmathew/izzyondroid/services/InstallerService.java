package in.sunilpaulmathew.izzyondroid.services;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.os.IBinder;

import in.sunilpaulmathew.sCommon.Utils.sInstallerUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class InstallerService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sInstallerUtils.setStatus(intent.getIntExtra(PackageInstaller.EXTRA_STATUS, -999), intent, this);
        stopSelf();
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}