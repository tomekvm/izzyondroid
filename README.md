# IzzyOnDroid

<p style="text-align: center"><img src="https://gitlab.com/sunilpaulmathew/izzyondroid/-/raw/master/fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" alt="" width="250" height="450" /> <img src="https://gitlab.com/sunilpaulmathew/izzyondroid/-/raw/master/fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" alt="" width="250" height="450" /> <img src="https://gitlab.com/sunilpaulmathew/izzyondroid/-/raw/master/fastlane/metadata/android/en-US/images/phoneScreenshots/7.png" alt="" width="250" height="450" /></p>

[![](https://img.shields.io/badge/IzzyOnDroid-v0.5-green)](https://apt.izzysoft.de/fdroid/index/apk/in.sunilpaulmathew.izzyondroid)

### An unofficial client for [IzzyOnDroid](https://apt.izzysoft.de/fdroid/repo) F-Droid Repository!

## Download
[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
alt="Get it on F-Droid"
height="80">](https://f-droid.org/packages/in.sunilpaulmathew.izzyondroid)
[<img src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png"
alt=""
height="80">](https://apt.izzysoft.de/fdroid/index/apk/in.sunilpaulmathew.izzyondroid)

## Features
* An elegantly designed user interface.
* Option to categorise applications.
* Includes an in-built APK installer.
* An auto-dark/light theme.
* A lot more.

## Support development

<p style="text-align: justify;">If you like to appreciate the efforts of the developer to provide this application entirely free, non-intrusive and without Ads, please consider supporting the development in some way. Maintaining this project takes a lot of time. So, each and every support from the android community will be hugely appreciated.</p>

<p><a href="https://liberapay.com/sunilpaulmathew/donate" target="_blank"><img src="https://liberapay.com/assets/widgets/donate.svg" alt="" height="60" /></a> <a href="https://www.paypal.me/menacherry" target="_blank"><img src="https://github.com/SmartPack/SmartPack.github.io/blob/master/asset/pic005.png?raw=true" alt="" height="60" /></a> <a href="https://ko-fi.com/sunilpaulmathew" target="_blank"><img src="https://github.com/SmartPack/SmartPack.github.io/blob/master/asset/pic010.png?raw=true" alt="" height="60" /></a> <a href="https://play.google.com/store/apps/details?id=com.smartpack.donate" target="_blank"><img src="https://github.com/SmartPack/SmartPack.github.io/blob/master/asset/pic009.png?raw=true" alt="" height="60" /></a></p>

<p><strong>Liberapay Donation url</strong>: <a href="https://liberapay.com/sunilpaulmathew/donate" target="_blank">https://liberapay.com/sunilpaulmathew/donate</a><br><strong>PayPal Donation url</strong>: <a href="https://www.paypal.me/menacherry" target="_blank">https://www.paypal.me/menacherry</a><br><strong>PayPal Donation  email</strong>: <a href="mailto:sunil.kde@gmail.com">sunil.kde@gmail.com</a><br><strong>Ko-fi Donation url</strong>: <a href="https://ko-fi.com/sunilpaulmathew" target="_blank">https://ko-fi.com/sunilpaulmathew</a></p>

Some other ways to support the development includes, but not limited to
<ol>
    <li>Contribute code to this project.</li>
    <li><a href="https://gitlab.com/sunilpaulmathew/izzyondroid/-/blob/master/app/src/main/res/values/strings.xml" target="_blank">Translate</a> this application into your local language (or improve existing translations).</li>
    <li>Share good words about this application with others (family, friends, and other enthusiastic android users).</li>
</ol>

## License

    Copyright (C) 2022-2023 sunilpaulmathew <sunil.kde@gmail.com>

    IzzyOnDroid is a free softwares: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at
    your option) any later version.

    IzzyOnDroid is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with IzzyOnDroid. If not, see <http://www.gnu.org/licenses/>.
